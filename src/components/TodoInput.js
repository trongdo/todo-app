import { React, useState } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  textField: {
    color: "white",
    textTransform: "capitalize",
  },
  gridItem: {
    alignSelf: "center",
  },
  button: {
    background:
      "linear-gradient(90deg, rgba(93, 12, 255, 1) 0%, rgba(155, 0, 250, 1) 100%)",
    padding: theme.spacing(1),
  },
  root: {
    "& label": {
      color: "#fff",
    },
    "& label.Mui-focused": {
      color: "rgba(93, 12, 255, 1)",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "rgba(93, 12, 255, 1)",
    },
    "& .MuiInput-input": {
      color: "#fff",
    },
  },
}));

function TodoInput({ handleAddTodo }) {
  const classes = useStyles();
  const [todoInput, setTodoInput] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const newTodo = {
      task: todoInput,
      isCompleted: false,
    };
    setTodoInput("");
    handleAddTodo(newTodo);
  };

  const handleOnChange = (e) => {
    setTodoInput(e.target.value);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Grid container spacing={0}>
          <Grid item xs={1} />
          <Grid className={classes.gridItem} item xs={8}>
            <TextField
              classes={{ root: classes.root }}
              id="todo-input"
              variant="standard"
              autoComplete="off"
              fullWidth
              label="What do you want to do?"
              value={todoInput}
              onChange={handleOnChange}
              color="secondary"
            />
          </Grid>
          <Grid className={classes.gridItem} item xs={2}>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              type="submit"
            >
              Add
            </Button>
          </Grid>
          <Grid item xs={1} />
        </Grid>
      </form>
    </div>
  );
}

export default TodoInput;
