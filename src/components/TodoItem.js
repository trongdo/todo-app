import React from "react";
import {
  Checkbox,
  IconButton,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { Check, CheckBoxOutlineBlankOutlined } from "@material-ui/icons";
import DeleteIcon from "@material-ui/icons/Delete";

const useStyles = makeStyles((theme) => ({
  paper: {
    background:
      "linear-gradient(90deg, rgba(93, 12, 255, 1) 0%, rgba(155, 0, 250, 1) 100%)",
    padding: theme.spacing(1),
    opacity: (todo) => (todo.isCompleted ? "50%" : "100%"),
  },
  gridItem: {
    alignSelf: "center",
  },
  typography: {
    fontWeight: "600",
    color: "#fff",
  },
  icon: {
    color: "#fff",
  },
}));

function TodoItem({ todo, handleDeleteTodo, handleUpdateTodo }) {
  const classes = useStyles(todo);

  const handleComplete = (todo) => {
    handleUpdateTodo(todo.id, { ...todo, isCompleted: !todo.isCompleted });
  };

  const handleDelete = (todo) => {
    handleDeleteTodo(todo.id);
  };

  return (
    <Grid item>
      <Paper className={classes.paper}>
        <Grid container spacing={1}>
          <Grid className={classes.gridItem} item xs={2} align="center">
            <Checkbox
              disableRipple
              checked={todo.isCompleted ? true : false}
              icon={<CheckBoxOutlineBlankOutlined className={classes.icon} />}
              checkedIcon={<Check className={classes.icon} />}
              onChange={() => handleComplete(todo)}
            />
          </Grid>
          <Grid className={classes.gridItem} item xs={8}>
            <Typography className={classes.typography} variant="body1">
              {todo.task}
            </Typography>
          </Grid>
          <Grid className={classes.gridItem} item xs={2} align="center">
            <IconButton
              disableFocusRipple
              disableRipple
              onClick={() => handleDelete(todo)}
            >
              <DeleteIcon className={classes.icon} />
            </IconButton>
          </Grid>
        </Grid>
      </Paper>
    </Grid>
  );
}

export default TodoItem;
