import { Grid } from "@material-ui/core";
import React from "react";
import TodoItem from "./TodoItem";

function TodoList({ todoList, handleDeleteTodo, handleUpdateTodo }) {
  return (
    <Grid container direction="column" spacing={2}>
      {todoList.map((todo) => (
        <TodoItem
          key={todo.id}
          todo={todo}
          handleDeleteTodo={handleDeleteTodo}
          handleUpdateTodo={handleUpdateTodo}
        />
      ))}
    </Grid>
  );
}

export default TodoList;
