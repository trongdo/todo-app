import * as todoAction from "../actions/TodoAction";

const initialState = {
  todoList: [],
};

export const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case todoAction.TODO_GET_ALL_SUCCESS:
      return {
        ...state,
        todoList: action.payload,
      };

    case todoAction.TODO_POST_SUCCESS:
      let addedTodoList = [...state.todoList, action.payload];
      return {
        ...state,
        todoList: addedTodoList,
      };

    case todoAction.TODO_DELETE_SUCCESS:
      const deletedTodoList = state.todoList.filter(
        (todo) => todo.id !== action.payload.id
      );
      return {
        ...state,
        todoList: deletedTodoList,
      };

    case todoAction.TODO_UPDATE_SUCCESS:
      const updatedTodoList = state.todoList.map((todo) => {
        if (todo.id === action.payload.id) {
          todo.isCompleted = action.payload.isCompleted;
          return todo;
        }
        return todo;
      });
      return {
        ...state,
        todoList: updatedTodoList,
      };

    default:
      return state;
  }
};
