import axiosClient from "./AxiosClient";

const todoApi = {
  getAll: () => {
    const url = "/TodoItems";
    return axiosClient.get(url);
  },
  add: (newTodo) => {
    const url = "/TodoItems";
    return axiosClient.post(url, newTodo);
  },
  delete: (id) => {
    const url = "TodoItems/" + id;
    return axiosClient.delete(url);
  },
  update: (id, newTodo) => {
    const url = "/TodoItems/" + id;
    return axiosClient.put(url, newTodo);
  },
};

export default todoApi;
