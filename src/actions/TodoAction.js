import todoApi from "../api/TodoApi";

export const TODO_POST_REQUEST = "TODO_POST_REQUEST";
export const TODO_POST_SUCCESS = "TODO_POST_SUCCESS";
//export const TODO_POST_ERROR = "TODO_POST_ERROR";

export const TODO_GET_ALL_REQUEST = "TODO_GET_ALL_REQUEST";
export const TODO_GET_ALL_SUCCESS = "TODO_GET_ALL_SUCCESS";
//export const TODO_GET_ALL_ERROR = "TODO_GET_ALL_ERROR";

export const TODO_DELETE_REQUEST = "TODO_DELETE_REQUEST";
export const TODO_DELETE_SUCCESS = "TODO_DELETE_SUCCESS";
//export const TODO_DELETE_ERROR = "TODO_DELETE_ERROR";

export const TODO_UPDATE_REQUEST = "TODO_UPDATE_REQUEST";
export const TODO_UPDATE_SUCCESS = "TODO_UPDATE_SUCCESS";
//export const TODO_UPDATE_ERROR = "TODO_UPDATE_ERROR";

export const addTodo = (todo) => {
  return async (dispatch) => {
    const res = await todoApi.add(todo);
    dispatch(addTodoSuccess(res.data));
  };
};

export const addTodoSuccess = (addedTodo) => {
  return {
    type: TODO_POST_SUCCESS,
    payload: addedTodo,
  };
};

export const getAllTodo = () => {
  return async (dispatch) => {
    const res = await todoApi.getAll();
    dispatch(getAllTodoSuccess(res.data));
  };
};

export const getAllTodoSuccess = (todoList) => {
  return {
    type: TODO_GET_ALL_SUCCESS,
    payload: todoList,
  };
};

export const deleteTodo = (id) => {
  return async (dispatch) => {
    const res = await todoApi.delete(id);
    dispatch(deleteTodoSuccess(res.data));
  };
};

export const deleteTodoSuccess = (deletedTodo) => {
  return {
    type: TODO_DELETE_SUCCESS,
    payload: deletedTodo,
  };
};

export const updateTodo = (id, newTodo) => {
  return async (dispatch) => {
    const res = await todoApi.update(id, newTodo);
    dispatch(updateTodoSuccess(res.data));
  };
};

export const updateTodoSuccess = (newTodo) => {
  return {
    type: TODO_UPDATE_SUCCESS,
    payload: newTodo,
  };
};
