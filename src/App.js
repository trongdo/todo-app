import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import TodoInput from "./components/TodoInput";
import TodoList from "./components/TodoList";
import {
  addTodo,
  deleteTodo,
  getAllTodo,
  updateTodo,
} from "./actions/TodoAction";
import { makeStyles, Container, Grid, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  container: {
    background: "#161a2b",
    minHeight: "600px",
    margin: "128px auto",
    borderRadius: "10px",
    paddingBottom: theme.spacing(3),
  },
  typography: {
    marginTop: theme.spacing(3),
    color: "#fff",
  },
}));

function App() {
  const classes = useStyles();
  const todoList = useSelector((state) => state.todo.todoList);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllTodo());
  }, [dispatch]);

  const handleAddTodo = (todo) => {
    dispatch(addTodo(todo));
  };

  const handleDeleteTodo = (id) => {
    dispatch(deleteTodo(id));
  };

  const handleUpdateTodo = (id, newTodo) => {
    dispatch(updateTodo(id, newTodo));
  };

  return (
    <Container className={classes.container} maxWidth="xs">
      <Grid container direction="column" spacing={3}>
        <Grid item>
          <Typography
            className={classes.typography}
            variant="h4"
            align="center"
          >
            Todo App
          </Typography>
        </Grid>
        <Grid item>
          <TodoInput handleAddTodo={handleAddTodo} />
        </Grid>
        <Grid item>
          <TodoList
            todoList={todoList}
            handleDeleteTodo={handleDeleteTodo}
            handleUpdateTodo={handleUpdateTodo}
          />
        </Grid>
      </Grid>
    </Container>
  );
}

export default App;
